package gladiatorharc;

import gladiatorok.Fegyvernem;
import gladiatorok.KepzettGladiator;
import gladiatorok.Szarmazas;
import java.util.ArrayList;
import java.util.Random;


public class Arena {
    private final int MAX_RESZTVEVOK=10;
    protected ArrayList<KepzettGladiator> gladiatorok;
    private Random rand;
    private String kommentar;

    public Arena(){
        gladiatorok=new ArrayList<KepzettGladiator>();
        rand=new Random();
        kommentar="";
    }

    private void harc(){
        int egyikHarcosIndexe= rand.nextInt(gladiatorok.size());
        int masikHarcosIndexe;
        do {
            masikHarcosIndexe = rand.nextInt(gladiatorok.size());
        }while(gladiatorok.get(egyikHarcosIndexe)==gladiatorok.get(masikHarcosIndexe));
        KepzettGladiator egyikGladiator=gladiatorok.get(egyikHarcosIndexe);
        KepzettGladiator masikGladiator=gladiatorok.get(masikHarcosIndexe);
        kommentar+=("\nPiros sarok:\n"+egyikGladiator.getBecenev()+","+
                egyikGladiator.getSzinkod()+","+egyikGladiator.getEv()+
                ", "+egyikGladiator.getSzarmazas());
        kommentar+=("\nKék sarok:\n"+masikGladiator.getBecenev()+","+
                masikGladiator.getSzinkod()+","+masikGladiator.getEv()+
                ", "+masikGladiator.getSzarmazas());

        if (egyikGladiator.erosebb(masikGladiator)==0){
            gladiatorok.remove(szavaz(egyikHarcosIndexe, masikHarcosIndexe));
        }else if (egyikGladiator.erosebb(masikGladiator)>0){
            kommentar+="\n"+(egyikGladiator.getBecenev()+" győzött\n\n");
            gladiatorok.remove(masikHarcosIndexe);
        }else if(egyikGladiator.erosebb(masikGladiator)<0){
                kommentar+="\n"+(masikGladiator.getBecenev()+" győzött\n\n");
                gladiatorok.remove(egyikHarcosIndexe);
        }
    }

    public String tornaIndul(boolean kommentar){
        valogat();
        while(gladiatorok.size()>1){
            harc();
        }
        KepzettGladiator tulelo=gladiatorok.get(0);
        tulelo.oregszik();
        if (tulelo.isSzabad()==true){
            gladiatorok.remove(0);
        }
        String eredmeny="neve: "+tulelo.getBecenev() + "\nkora: " + tulelo.getEv() +
                "\nszinkodja: " + tulelo.getSzinkod() + "\nszarmazasa: " + tulelo.getSzarmazas() +
                "\nszabad e?:" + tulelo.isSzabad()+
                (tulelo.isSzabad()==true?"\n"+tulelo.getBecenev()+" nyugdijba vonul.":"\n"+tulelo.getBecenev()+" továbbjutott.");
        if (kommentar) {
            return this.kommentar + eredmeny;
        }
        return eredmeny;
    }


    private int szavaz(int egyikGladiatorIndex, int masikGladiatorIndex){
        int[] kuzdoFelek={egyikGladiatorIndex, masikGladiatorIndex};
        int nyertesIndex=rand.nextInt(kuzdoFelek.length);
        kommentar+="\n"+(gladiatorok.get(nyertesIndex).getBecenev()+" győzött szavazat alapján\n\n");
        return kuzdoFelek[nyertesIndex];
    }

    private String getRandomName(int minHossz, int maxHossz){
        char[] validCharacters={'a','á', 'b', 'c', 'd', 'e', 'é', 'f', 'g', 'h', 'i', 'í',  'j', 'k', 'l',
        'm', 'n', 'o', 'ó', 'ö', 'ő',  'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        int nevHossz=rand.nextInt(maxHossz+1-minHossz)+minHossz;
        StringBuilder stringBuilder=new StringBuilder();
        String nev="";
        for (int i = 0; i <nevHossz ; i++) {
            stringBuilder.append(validCharacters[rand.nextInt(validCharacters.length)]);
        }
        nev=stringBuilder.toString().substring(0,1).toUpperCase()+stringBuilder.substring(1);
        return nev;
    }

    public void valogat(){
        while(gladiatorok.size()<MAX_RESZTVEVOK){
            int ev=rand.nextInt(10);
            Szarmazas randomSzarmazas=Szarmazas.values()[rand.nextInt(Szarmazas.values().length)];
            Fegyvernem randomFegyvernem=Fegyvernem.values()[rand.nextInt(Fegyvernem.values().length)];

            String becenev=getRandomName(3,15);

            try {
                this.gladiatorok.add(new KepzettGladiator(ev, randomSzarmazas, randomFegyvernem, becenev));
            }catch(IllegalArgumentException e){
                kommentar+=becenev+" "+e.getMessage()+"("+ev+" éves)\n";
            }

        }
    }
}
