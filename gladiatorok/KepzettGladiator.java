package gladiatorok;

public class KepzettGladiator extends Gladiator {
    private Fegyvernem fegyvernem;
    private String becenev;

    public KepzettGladiator(int ev, Szarmazas szarmazas, Fegyvernem fegyvernem, String becenev){
        super(ev, szarmazas);
        this.fegyvernem=fegyvernem;
        if (becenev.length()>=3 && becenev.length()<=15){
            this.becenev=becenev;
        }else{
            throw new IllegalArgumentException("A becenév nem megfelelő hosszúságú(3<=becenév<=15)");
        }
    }

    public String getBecenev() {
        return becenev;
    }

    @Override
    public void oregszik() {
        super.oregszik();
        if (this.getEv()>=6){
            super.oregszik();
        }
    }
}
