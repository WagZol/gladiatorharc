/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gladiatorok;

/**
 *
 * @author nagy.gabor
 */
public class Gladiator {
    
    private int ev;
    private boolean szabad;
    private Szinkod szinkod;
    private Szarmazas szarmazas;

    public Gladiator(int ev, Szarmazas szarmazas) {
        if (ev<=5 && ev>=0) {
            this.ev = ev;
        }else{
            throw new IllegalArgumentException("túl öreg.");
        }
       
        this.szarmazas = szarmazas;
        szabad=false;
        fejlodik();
    }
    
    

    public int getEv() {
        return ev;
    }

    public boolean isSzabad() {
        return szabad;
    }

    public Szinkod getSzinkod() {
        return szinkod;
    }

    public Szarmazas getSzarmazas() {
        return szarmazas;
    }

    private void fejlodik() {
        if (this.ev>=6){
            this.szinkod=Szinkod.FEKETE;
        }else if(this.ev>=3){
            this.szinkod=Szinkod.PIROS;
        }else{
            this.szinkod=Szinkod.SARGA;
        }
    }

    public int erosebb(KepzettGladiator kepzettGladiator){
        return this.szinkod.getSzint()-kepzettGladiator.getSzinkod().getSzint();
    }

    private void felszabadit(){
        this.szabad=true;
    }

    protected void oregszik(){
        if (this.ev<=9){
            this.ev++;
            fejlodik();
        }else{
            felszabadit();
        }
    }
    
    
    
    
    
}
