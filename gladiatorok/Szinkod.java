/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gladiatorok;

/**
 *
 * @author nagy.gabor
 */
public enum Szinkod {
    SARGA (0),
    PIROS (1),
    FEKETE (2);

    private final int szint;

    private Szinkod(int szint){
        this.szint=szint;
    }

    public int getSzint(){
        return this.szint;
    }
}
